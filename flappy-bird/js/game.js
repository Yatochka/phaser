(function(){
    var GameInitialize = function GameInitialize(){

        //Game Constants//
        var DEBUG_MODE = true,
            SPEED = 180,
            GRAVITY = 1800,
            BIRD_FLAP = 550,
            PIPE_SPAWN_MIN_INTERVAL = 1200,
            PIPE_SPAWN_MAX_INTERVAL = 3000,
            AVALIBLE_SPACE_BETWEEN_PIPE = 300,

            CLOUDS_SHOW_MIN_TIME = 3000,
            CLOUDS_SHOW_MAX_TIME = 5000,
            MAX_DIFFICULT = 100,

            SCENE = '',

            TITLE_TEXT = "FLAPPY BIRD",
            HIGHSCORE_TITLE = "HIGHSCORES",
            HIGHSCORE_SUBMIT = "POST SCORE", 

            INSTRUCTIONS_TEXT = "PRESS \nSPACE \nTO \nFLY",
            DEV_TEXT = "DEVELOPER WITH HELP \nHabr tuturial",

            GRAPHIC_TEXT = "Graphic\nDmitry Lezhenko\nHabr tutorial",
            LOADING_TEXT = "LOADING...",
            WINDOW_WIDTH = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body'[0].clientWidth),
            WINDOW_HEIGHT = window.innerHeight || document.documentElement.clientHeight || document.getElementsByTagName('body'[0].clientHeight);

        //Vars for Saving Objects//
        var Background,
            Clouds,CloudsTimer,
            Pipes,PipesTimer,FreeSpaceInPipes,
            Bird,
            Town,
            FlapSound,ScoreSound,HurtSound,
            TitleText,DeveloperText,GhaphicText,ScoreText,InstructionsText,HighScoreTitleText,HighScoreText,PostScoreText,LoadingText,
            PostScoreClickArea,
            isScorePosted = false,
            isSoundEnable = false,
            Leaderboard;

        //State - BootGame//
        var BootGameState = new Phaser.State();
            BootGameState = function(){
                LoadingText = Game.add.text(Game.world.width / 2, Game.world.height / 2, LOADING_TEXT, {
                    font: '32px "Press Start 2P"',
                    fill: '#FFFFFF',
                    stroke: '#000000',
                    alight: 'center',
                    strokeThickness: 3
                });
                LoadingText.anchor.setTo(0.5, 0.5);
                Game.state.start('Preloader',false, false);
            }

        //State - Preloader//
        
        
        var Game = new Phaser.Game(WINDOW_WIDTH, WINDOW_HEIGHT, Phaser.CANVAS, SCENE);

        Game.raf = new Phaser.RequestAnimationFrame(Game);
        Game.antialias = false;
        Game.raf.start();

        Game.state.add('Boot', BootGameState, false);
        Game.state.add('Preloader', PreloaderState, false);
        Game.state.add('MainMenu',MainMenuState, false);
        Game.state.add('Game',GameState, false);
        Game.state.add('GameOver',GameOverState, false);

        Game.state.start('Boot');

        Clay.ready(function(){
            Leaderboard = new Clay.Leaderboard({
                id: 1337
            });
        });





    }
});

WebFont.load({
    google: {
        families: ['Press+Start+2P']
    },
    active: function() {
        GameInitialize();
    }
});

